#include <iostream>


void FuncEven(bool a, int N) // if a == 0 - even, if a == 1 - odd
{
	int i = 1;
	while (i <= N)
	{
		if (i % 2 == a) {

			std::cout << i << " ";
		}
		
		++i;
	}
 

}


int main()
{
	int N = 22;
	FuncEven(false, N);
}
